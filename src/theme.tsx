const blackGoldTheme = {
  BACKGROUND_COLOR: "#1a1a1a",
  TEXT_COLOR: "#F8F8F8",
  HIGHLIGHT_COLOR: "gold",
  BUTTON_TEXT_COLOR:"#000",
  BUTTON_DISABLED_TEXT_COLOR:"#333",
  BUTTON_ENABLED:"gold",
  BUTTON_DISABLED:"#666"
};
const silverTheme = {
    BACKGROUND_COLOR: "#e0e0e0",
    TEXT_COLOR: "#1a1a1a",
    HIGHLIGHT_COLOR: "#000",
    BUTTON_TEXT_COLOR:"#000",
    BUTTON_ENABLED:"#FFF",
    BUTTON_DISABLED:"#333"
  };

const Theme = blackGoldTheme;

export default Theme;
